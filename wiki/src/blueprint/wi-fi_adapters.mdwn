USB Wi-Fi adapters that don't work in Tails
===========================================

Related tickets: [[!tails_ticket 15131]]

WikiDevi has lots of information about Wi-Fi adapters:
<https://wikidevi.com/>

The list of Wi-Fi adapters that work in Tails is in our documentation
about [[Troubleshooting Wi-Fi not
working|doc/anonymous_internet/networkmanager#wi-fi-adapters]].

<table>
<tr><th>Vendor</th><th>Model</th><th>Year</th><th>Size</th><th>lsusb ID</th><th>Latest test</th><th>Module</th><th>Firmware</th><th>Note</th></tr>
<tr><td>D-Link</td><td><a href="https://wikidevi.com/wiki/D-Link_DWA-171_rev_A1">DWA-171 rev A1</a></td><td>Late 2017</td><td>Small</td><td>2001:3314</td><td>4.0</td><td>None</td><td>None</td><td></td></tr>
<tr><td>D-Link</td><td><a href="https://wikidevi.com/wiki/D-Link_DWA-131_rev_E1">DWA-131E1 rev E1</a></td><td>Early 2018</td><td>Small</td><td>2001:3319</td><td>4.0</td><td>rtl8xxxu</td><td>rtl8192eu_nic.bin</td><td>Authentication failures</td></tr>
<tr><td>Linksys</td><td><a href="https://wikidevi.com/wiki/Linksys_WUSB6100M">WUSB6100M</a></td><td>Late 2017</td><td>Small</td><td>13b1:0042</td><td>4.0</td><td>None</td><td>None</td><td></td></tr>
<tr><td>TP-Link</td><td>TL-WN722N</td><td>Late 2019</td><td>Long with antenna</td><td>2357:010c</td><td>4.0</td><td>r8188eu</td><td>???</td><td></td></tr>
<tr><td>TP-Link</td><td><a href="https://wikidevi.com/wiki/TP-LINK_TL-WN725N_v2">TL-WN725N v2</a></td><td>Early 2017</td><td>Nano</td><td>0bda:8179</td><td>4.0</td><td>r8188eu</td><td>???</td><td>MAC spoofing fails</td></tr>
<tr><td>TP-Link</td><td><a href="https://wikidevi.com/wiki/TP-LINK_TL-WN823N_v2">TL-WN823N v2</a></td><td>Late 2017</td><td>Small</td><td>2357:0109</td><td>4.0</td><td>rtl8xxxu</td><td>rtl8192eu_nic.bin</td><td>Authentication failures. Version 1 worked.</td></tr>
<tr><td>TP-Link</td><td><a href="https://wikidevi.com/wiki/TP-LINK_TL-WN821N_v6">TL-WN821N v6</a></td><td>Early 2018</td><td>Long</td><td>2357:0107</td><td>4.0</td><td>None</td><td>None</td><td></td></tr>
<tr><td>TP-Link</td><td>Archer T2U Nano</td><td>Late 2019</td><td>Nano</td><td>2357:011e</td><td>4.0</td><td>None</td><td>None</td><td></td></tr>
<tr><td>TP-Link</td><td>Archer T2U</td><td>Early 2020</td><td>Small</td><td>2357:011f</td><td>4.4</td><td>None</td><td>None</td><td></td></tr>
<!--
<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
-->
</table>

Wi-Fi adapters that might work
==============================

- [TP-Link Archer T2UH](https://static.tp-link.com/2018/201810/20181022/Archer%20T2UH\(EU&US\)_2.0_datasheet.pdf) (late 2017) now works in 4.0 (FCC ID: TE7T2UH, IC: 8853A-T2UH)

Other USB Wi-Fi adapters that work in Tails but have no advantage
=================================================================

- [CanaKit Raspberry Pi](https://www.amazon.com/d/B00GFAN498) (same size & price as Edimax)
- [Panda Wireless Mini](https://www.amazon.com/d/B003283M6Q) (larger & more expensive)
- [Panda Wireless N600](https://www.amazon.com/d/B00U2SIS0O) (larger & more expensive)
- [TP-Link TL-WN723N](https://www.tp-link.com/us/home-networking/usb-adapter/tl-wn723n/) (end of life)
