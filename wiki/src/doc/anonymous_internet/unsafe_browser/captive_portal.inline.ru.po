# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-08-29 08:06+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <p>
msgid ""
"Many publicly accessible Internet connections (usually available through a "
"wireless network connection) require users to first log in to a <em>captive "
"portal</em> in order to access the Internet."
msgstr ""

#. type: Content of: <p>
msgid ""
"A captive portal is a web page that is displayed to the user before the user "
"can access the Internet. Captive portals usually require the user to log in "
"to the network or enter information such as an email address. Captive "
"portals are commonly encountered at Internet cafés, libraries, airports, "
"hotels, and universities."
msgstr ""

#. type: Content of: <p>
msgid ""
"This is an example of a captive portal (by <a href=\"https://commons."
"wikimedia.org/wiki/File:Captive_Portal.png\">AlexEng</a>):"
msgstr ""

#. type: Content of: outside any tag (error?)
msgid ""
"[[!img captive-portal.png link=\"no\" alt=\"Welcome! Please enter your "
"credentials to connect.\"]]"
msgstr ""
