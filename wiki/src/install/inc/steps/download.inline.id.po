# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2020-04-23 16:49+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Content of: <div>
msgid "2.4"
msgstr ""

#. type: Content of: <div>
msgid "[[!inline pages=\"inc/stable_amd64_version\" raw=\"yes\" sort=\"age\"]]"
msgstr ""

#. type: Content of: <h1>
msgid ""
"Download Tails [[!inline pages=\"inc/stable_amd64_version\" raw=\"yes\" sort="
"\"age\"]]"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"While you are downloading, we recommend you read the [[release notes|doc/"
"upgrade/release_notes]] for Tails [[!inline pages=\"inc/stable_amd64_version"
"\" raw=\"yes\" sort=\"age\"]]<span class=\"remove-extra-space\">.</span> "
"They document all the changes in this new version: new features, problems "
"that were solved, and known issues that have already been identified."
msgstr ""

#. type: Content of: <div><div><h2>
msgid "Direct download"
msgstr ""

#. type: Content of: <div><div><div><div><h3>
msgid ""
"<span class=\"step-number\"><span class=\"usb upgrade\">1.</span>1</"
"span>Download Tails"
msgstr ""

#. type: Content of: <div><div><div><div><div>
msgid ""
"<a href=\"[[!inline pages=\"inc/stable_amd64_img_url\" raw=\"yes\" sort=\"age"
"\"]]\" id=\"download-img\" class=\"use-mirror-pool btn btn-primary inline-"
"block indent\">Download Tails [[!inline pages=\"inc/stable_amd64_version\" "
"raw=\"yes\" sort=\"age\"]] USB image (<span class=\"remove-extra-space\">[[!"
"inline pages=\"inc/stable_amd64_img_size\" raw=\"yes\" sort=\"age\"]]</"
"span>)</a> <a href=\"[[!inline pages=\"inc/stable_amd64_img_url\" raw=\"yes"
"\" sort=\"age\"]]\" id=\"download-img\" class=\"use-mirror-pool-on-retry btn "
"btn-primary inline-block indent\">Download Tails [[!inline pages=\"inc/"
"stable_amd64_version\" raw=\"yes\" sort=\"age\"]] USB image (<span class="
"\"remove-extra-space\">[[!inline pages=\"inc/stable_amd64_img_size\" raw="
"\"yes\" sort=\"age\"]]</span>)</a>"
msgstr ""

#. type: Content of: <div><div><div><div><div>
msgid ""
"<a href=\"[[!inline pages=\"inc/stable_amd64_iso_url\" raw=\"yes\" sort=\"age"
"\"]]\" id=\"download-iso\" class=\"use-mirror-pool btn btn-primary inline-"
"block indent\">Download Tails [[!inline pages=\"inc/stable_amd64_version\" "
"raw=\"yes\" sort=\"age\"]] ISO image (<span class=\"remove-extra-space\">[[!"
"inline pages=\"inc/stable_amd64_iso_size\" raw=\"yes\" sort=\"age\"]]</"
"span>)</a> <a href=\"[[!inline pages=\"inc/stable_amd64_iso_url\" raw=\"yes"
"\" sort=\"age\"]]\" id=\"download-iso\" class=\"use-mirror-pool-on-retry btn "
"btn-primary inline-block indent\">Download Tails [[!inline pages=\"inc/"
"stable_amd64_version\" raw=\"yes\" sort=\"age\"]] ISO image (<span class="
"\"remove-extra-space\">[[!inline pages=\"inc/stable_amd64_iso_size\" raw="
"\"yes\" sort=\"age\"]]</span>)</a>"
msgstr ""

#. type: Content of: <div><div><div><div><div><p>
msgid ""
"<a>I already downloaded Tails&nbsp;<span class=\"remove-extra-space\">[[!"
"inline pages=\"inc/stable_amd64_version\" raw=\"yes\" sort=\"age\"]]</span>."
"</a>"
msgstr ""

#. type: Content of: <div><div><div><div><div><p>
msgid ""
"If the download fails, try to <a href=\"[[!inline pages=\"inc/"
"stable_amd64_img_url\" raw=\"yes\" sort=\"age\"]]\" class=\"usb upgrade "
"download-only-img\">download from another mirror.</a> <a href=\"[[!inline "
"pages=\"inc/stable_amd64_iso_url\" raw=\"yes\" sort=\"age\"]]\" class=\"dvd "
"vm download-only-iso\">download from another mirror.</a>"
msgstr ""

#. type: Content of: <div><div><div><div><h3>
msgid ""
"<span class=\"step-number\"><span class=\"usb upgrade\">1.</span>2</"
"span>Verify your download using your browser"
msgstr ""

#. type: Content of: <div><div><div><div><p><b>
msgid "<b>For your security,"
msgstr ""

#. type: Content of: <div><div><div><div><p>
msgid "always verify your download!</b>"
msgstr ""

#. type: Content of: <div><div><div><div><div><p>
msgid "[[!toggle id=\"why-verify-supported\" text=\"Why?\"]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><div>
msgid ""
"[[!toggleable id=\"why-verify-supported\" text=\"\"\" [[!toggle id=\"why-"
"verify-supported\" text=\"X\"]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><p>
msgid "With an unverified download, you might:"
msgstr ""

#. type: Content of: <div><div><div><div><div><ul><li>
msgid ""
"Lose time if your download is incomplete or broken due to an error during "
"the download.  This is quite frequent."
msgstr ""

#. type: Content of: <div><div><div><div><div><ul><li>
msgid ""
"Get hacked while using Tails if our download mirrors have been compromised "
"and are serving malicious downloads."
msgstr ""

#. type: Content of: <div><div><div><div><div><ul><li>
msgid ""
"<a href=\"http://blog.linuxmint.com/?p=2994\">This already happened to other "
"operating systems.</a>"
msgstr ""

#. type: Content of: <div><div><div><div><div><ul><li>
msgid ""
"Get hacked while using Tails if your download is modified on the fly by an "
"attacker on the network."
msgstr ""

#. type: Content of: <div><div><div><div><div><ul><li>
msgid ""
"<a href=\"https://en.wikipedia.org/wiki/DigiNotar\">This is possible for "
"strong adversaries.</a>"
msgstr ""

#. type: Content of: <div><div><div><div><div><p>
msgid ""
"[[How does the extension work?|contribute/design/verification_extension]]"
msgstr ""

#. type: Content of: <div>
msgid "\"\"\"]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><p>
msgid "Our browser extension makes it quick and easy."
msgstr ""

#. type: Content of: <div><div><div><div><div>
msgid ""
"<a href=\"https://addons.mozilla.org/firefox/downloads/latest/tails-"
"verification/addon-tails-verification-latest.xpi\" class=\"install-extension-"
"btn supported-browser firefox btn btn-primary inline-block\">Install "
"<u>Tails Verification</u> extension</a> <a href=\"https://chrome.google.com/"
"webstore/detail/tails-verification/gaghffbplpialpoeclgjkkbknblfajdl\" class="
"\"install-extension-btn supported-browser chrome btn btn-primary inline-block"
"\" target=\"_blank\">Install <u>Tails Verification</u> extension</a>"
msgstr ""

#. type: Content of: <div><div><div><div><div><div><p>
msgid ""
"You seem to have JavaScript disabled. To use our browser extension, please "
"allow all this page:"
msgstr ""

#. type: Content of: <div><div><div><div><div><div>
msgid "[[!img screenshots/allow_js.png link=\"no\"]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><p>
msgid "Your extension is an older version."
msgstr ""

#. type: Content of: <div><div><div><div><div>
msgid ""
"<a href=\"https://addons.mozilla.org/firefox/downloads/latest/tails-"
"verification/addon-tails-verification-latest.xpi\" class=\"install-extension-"
"btn firefox btn btn-primary inline-block\">Update extension</a> <a href="
"\"https://chrome.google.com/webstore/detail/tails-verification/"
"gaghffbplpialpoeclgjkkbknblfajdl\" class=\"install-extension-btn chrome btn "
"btn-primary inline-block\" target=\"_blank\">Update extension</a>"
msgstr ""

#. type: Content of: <div><div><div><div><div><p>
msgid "<u>Tails Verification</u> extension installed!"
msgstr ""

#. type: Content of: <div><div><div><div><div>
msgid ""
"<label id=\"verify-download-wrapper\" class=\"btn btn-primary inline-block"
"\"> Verify Tails <span class=\"remove-extra-space\">&nbsp;[[!inline pages="
"\"inc/stable_amd64_version\" raw=\"yes\" sort=\"age\"]]</span>&hellip; "
"<input id=\"verify-download\" type=\"file\"/> </label>"
msgstr ""

#. type: Content of: <div><div><div><div><div><div><p>
msgid "Verifying <span id=\"filename\">$FILENAME</span>&hellip;"
msgstr ""

#. type: Content of: <div><div><div><div><div><p>
msgid "Verification successful!"
msgstr ""

#. type: Content of: <div><div><div><div><div><div><p>
msgid "<b>Verification failed!</b>"
msgstr ""

#. type: Content of: <div><div><div><div><div><div><p>
msgid "[[!toggle id=\"why-failed\" text=\"Why?\"]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><div><div>
msgid ""
"[[!toggleable id=\"why-failed\" text=\"\"\" [[!toggle id=\"why-failed\" text="
"\"X\"]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><div><div><p>
msgid ""
"Most likely, the verification failed because of an error or interruption "
"during the download."
msgstr ""

#. type: Content of: <div><div><div><div><div><div><div><p>
msgid ""
"The verification also fails if you try to verify a different download than "
"the latest version (<span class=\"remove-extra-space\">[[!inline pages=\"inc/"
"stable_amd64_version\" raw=\"yes\" sort=\"age\"]]</span>)."
msgstr ""

#. type: Content of: <div><div><div><div><div><div><div><p>
msgid ""
"Less likely, the verification might have failed because of a malicious "
"download from our download mirrors or due to a network attack in your "
"country or local network."
msgstr ""

#. type: Content of: <div><div><div><div><div><div><div><p>
msgid "Downloading again is usually enough to fix this problem."
msgstr ""

#. type: Content of: <div><div><div><div><div><div><p>
msgid ""
"<a href=\"[[!inline pages=\"inc/stable_amd64_img_url\" raw=\"yes\" sort=\"age"
"\"]]\" id=\"download-img-again\" class=\"use-mirror-pool-on-retry\">Please "
"try to download again&hellip;</a>"
msgstr ""

#. type: Content of: <div><div><div><div><div><div><p>
msgid ""
"<a href=\"[[!inline pages=\"inc/stable_amd64_iso_url\" raw=\"yes\" sort=\"age"
"\"]]\" id=\"download-iso-again\" class=\"use-mirror-pool-on-retry\">Please "
"try to download again&hellip;</a>"
msgstr ""

#. type: Content of: <div><div><div><div><div><div><p>
msgid "<b>Verification failed again!</b>"
msgstr ""

#. type: Content of: <div><div><div><div><div><div><p>
msgid "[[!toggle id=\"why-failed-again\" text=\"Why?\"]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><div><div>
msgid ""
"[[!toggleable id=\"why-failed-again\" text=\"\"\" [[!toggle id=\"why-failed-"
"again\" text=\"X\"]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><div><div><p>
msgid "The verification might have failed again because of:"
msgstr ""

#. type: Content of: <div><div><div><div><div><div><div><ul><li>
msgid "A software problem in our verification extension"
msgstr ""

#. type: Content of: <div><div><div><div><div><div><div><ul><li>
msgid "A malicious download from our download mirrors"
msgstr ""

#. type: Content of: <div><div><div><div><div><div><div><ul><li>
msgid "A network attack in your country or local network"
msgstr ""

#. type: Content of: <div><div><div><div><div><div><div><p>
msgid ""
"Trying from a different place or a different computer might solve any of "
"these issues."
msgstr ""

#. type: Content of: <div><div><div><div><div><div><p>
msgid ""
"Please try to download again from a different place or a different "
"computer&hellip;"
msgstr ""

#. type: Content of: <div><div><div><h3>
msgid ""
"<span class=\"step-number\"><span class=\"usb upgrade\">1.</span>3</"
"span>Continue <span class=\"usb\">installing</span> <span class=\"upgrade"
"\">upgrading</span> <span class=\"download-only-img download-only-iso"
"\">installing or upgrading</span>"
msgstr ""

#. type: Content of: <div><div><div><div>
msgid ""
"<span class=\"windows\">[[Skip download|win/usb]]</span> <span class=\"linux"
"\">[[Skip download|linux/usb]]</span> <span class=\"mac\">[[Skip download|"
"mac/usb]]</span> <span class=\"dvd\">[[Skip download|dvd]]</span> <span "
"class=\"vm\">[[Skip download|doc/advanced_topics/virtualization]]</span> "
"<span class=\"upgrade-tails\">[[Skip download|upgrade/tails]]</span> <span "
"class=\"upgrade-windows\">[[Skip download|upgrade/win]]</span> <span class="
"\"upgrade-mac\">[[Skip download|upgrade/mac]]</span> <span class=\"upgrade-"
"linux\">[[Skip download|upgrade/linux]]</span> <span class=\"download-only-"
"img download-only-iso\">[[Skip download|doc#install]]</span>"
msgstr ""

#. type: Content of: <div><div><div><div><div><div>
msgid "[[Skip verification!|win/usb]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><div>
msgid "[[Skip verification!|linux/usb]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><div>
msgid "[[Skip verification!|mac/usb]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><div>
msgid "[[Skip verification!|dvd]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><div>
msgid "[[Skip verification!|doc/advanced_topics/virtualization]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><div>
msgid "[[Skip verification!|upgrade/tails]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><div>
msgid "[[Skip verification!|upgrade/win]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><div>
msgid "[[Skip verification!|upgrade/mac]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><div>
msgid "[[Skip verification!|upgrade/linux]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><div>
msgid "[[Skip verification|doc#install]]"
msgstr ""

#. type: Content of: <div><div><div><div><div>
msgid "[["
msgstr ""

#. type: Content of: <div><div><div><div><div><div>
msgid "Next: Install Tails (<span class=\"next-counter\"></span>)"
msgstr ""

#. type: Content of: <div><div><div><div><div>
msgid "|win/usb]]"
msgstr ""

#. type: Content of: <div><div><div><div><div>
msgid "|linux/usb]]"
msgstr ""

#. type: Content of: <div><div><div><div><div>
msgid "|mac/usb]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><div>
msgid ""
"Next: Install an intermediary Tails (<span class=\"next-counter\"></span>)"
msgstr ""

#. type: Content of: <div><div><div><div><div>
msgid "|upgrade/tails]]"
msgstr ""

#. type: Content of: <div><div><div><div><div>
msgid "|upgrade/win]]"
msgstr ""

#. type: Content of: <div><div><div><div><div>
msgid "|upgrade/mac]]"
msgstr ""

#. type: Content of: <div><div><div><div><div>
msgid "|upgrade/linux]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><div>
msgid "Next: Burning Tails on a DVD"
msgstr ""

#. type: Content of: <div><div><div><div><div>
msgid "|dvd]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><div>
msgid "Next: Virtualization"
msgstr ""

#. type: Content of: <div><div><div><div><div>
msgid "|doc/advanced_topics/virtualization]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><p>
msgid "Upgrade your Tails USB stick and keep your Persistent Storage:"
msgstr ""

#. type: Content of: <div><div><div><div><div><ul><li>
msgid "[[Upgrade from your Tails|upgrade/tails]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><ul><li>
msgid "[[Upgrade from Windows|upgrade/win]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><ul><li>
msgid "[[Upgrade from macOS|upgrade/mac]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><ul><li>
msgid "[[Upgrade from Linux|upgrade/linux]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><p>
msgid "Install a new USB stick:"
msgstr ""

#. type: Content of: <div><div><div><div><div><ul><li>
msgid "[[Install from Windows|install/win/usb]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><ul><li>
msgid "[[Install from macOS|install/mac/usb]]"
msgstr ""

#. type: Content of: <div><div><div><div><div><ul><li>
msgid "[[Install from Linux|install/linux/usb]]"
msgstr ""

#. type: Content of: <div><div><div><div><ul><li>
msgid "[[Burn on a DVD|dvd]]"
msgstr ""

#. type: Content of: <div><div><div><div><ul><li>
msgid "[[Run in a virtual machine|doc/advanced_topics/virtualization]]"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid ""
"You are using <u><b><span id=\"detected-browser\">$DETECTED-BROWSER</span></"
"b></u>."
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "Direct download is only available for:"
msgstr ""

#. type: Content of: <div><div><div><ul><li>
msgid ""
"Firefox <span id=\"min-version-firefox\">$MINVER-FIREFOX</span> and later "
"(<a href=\"https://www.mozilla.org/firefox/new/\">Download</a>)"
msgstr ""

#. type: Content of: <div><div><div><ul><li>
msgid ""
"Chrome<span id=\"min-version-chrome\">$MINVER-CHROME</span> and later (<a "
"href=\"https://www.google.com/chrome/\">Download</a>)"
msgstr ""

#. type: Content of: <div><div><div><ul><li>
msgid ""
"Tor Browser <span id=\"min-version-tor-browser\">$MINVER-TOR-BROWSER</span> "
"and later (<a href=\"https://www.torproject.org/download/download-easy.html"
"\">Download</a>)"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "Please update your browser to the latest version."
msgstr ""

#. type: Content of: <div><div><div><div><p>
msgid "[[!toggle id=\"why-verify-unsupported\" text=\"Why?\"]]"
msgstr ""

#. type: Content of: <div><div><div><div><div>
msgid ""
"[[!toggleable id=\"why-verify-unsupported\" text=\"\"\" [[!toggle id=\"why-"
"verify-unsupported\" text=\"X\"]]"
msgstr ""

#. type: Content of: <div><div><div><div><p>
msgid ""
"Our browser extension for Firefox, Chrome, and Tor Browser makes this quick "
"and easy."
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "Copy and paste this link in Firefox, Chrome, or Tor Browser:"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<code>https://tails.boum.org/install/win/usb-download/</code>"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<code>https://tails.boum.org/install/linux/usb-download/</code>"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<code>https://tails.boum.org/install/mac/usb-download/</code>"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<code>https://tails.boum.org/upgrade/tails-download/</code>"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<code>https://tails.boum.org/upgrade/win-download/</code>"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<code>https://tails.boum.org/upgrade/mac-download/</code>"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<code>https://tails.boum.org/upgrade/linux-download/</code>"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<code>https://tails.boum.org/install/dvd-download/</code>"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<code>https://tails.boum.org/install/vm-download/</code>"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<code>https://tails.boum.org/install/download/</code>"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "<code>https://tails.boum.org/install/download-iso/</code>"
msgstr ""

#. type: Content of: <div><div><h2>
msgid "BitTorrent download"
msgstr ""

#. type: Content of: <div><div><p>
msgid "[[!toggle id=\"what-is-bittorrent\" text=\"What is BitTorrent?\"]]"
msgstr ""

#. type: Content of: <div><div><div>
msgid ""
"[[!toggleable id=\"what-is-bittorrent\" text=\"\"\" [[!toggle id=\"what-is-"
"bittorrent\" text=\"X\"]]"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid ""
"BitTorrent is a peer-to-peer technology for file sharing that makes your "
"download faster and easier to resume."
msgstr ""

#. type: Content of: <div><div><div><p>
msgid ""
"You need to install BitTorrent software on your computer, like <a href="
"\"https://transmissionbt.com/\">Transmission</a> (for Windows, macOS, and "
"Linux)."
msgstr ""

#. type: Content of: <div><div><div><p>
msgid "BitTorrent doesn't work over Tor or in Tails."
msgstr ""

#. type: Content of: <div><div><div><h3>
msgid ""
"<span class=\"step-number\"><span class=\"usb upgrade\">1.</span>1</"
"span>Download Tails (Torrent file)"
msgstr ""

#. type: Content of: <div><div><div><div>
msgid ""
"<a href=\"[[!inline pages=\"inc/stable_amd64_img_torrent_url\" raw=\"yes\" "
"sort=\"age\"]]\" id=\"download-img-torrent\" class=\"btn btn-primary inline-"
"block indent\">Download Tails [[!inline pages=\"inc/stable_amd64_version\" "
"raw=\"yes\" sort=\"age\"]] Torrent file for USB image</a>"
msgstr ""

#. type: Content of: <div><div><div><div>
msgid ""
"<a href=\"[[!inline pages=\"inc/stable_amd64_iso_torrent_url\" raw=\"yes\" "
"sort=\"age\"]]\" id=\"download-iso-torrent\" class=\"btn btn-primary inline-"
"block indent\">Download Tails [[!inline pages=\"inc/stable_amd64_version\" "
"raw=\"yes\" sort=\"age\"]] Torrent file for ISO image</a>"
msgstr ""

#. type: Content of: <div><div><div><h3>
msgid ""
"<span class=\"step-number\"><span class=\"usb upgrade\">1.</span>2</"
"span>Verify your download using BitTorrent"
msgstr ""

#. type: Content of: <div><div><div><p>
msgid ""
"Your BitTorrent client will automatically verify your download when it is "
"complete."
msgstr ""

#. type: Content of: <div><div><div><p>
msgid ""
"Open and download the Torrent file with your BitTorrent client. It contains "
"the Tails [[!inline pages=\"inc/stable_amd64_version\" raw=\"yes\" sort=\"age"
"\"]] <span class=\"usb upgrade download-only-img\">USB</span> <span class="
"\"dvd vm download-only-iso\">ISO</span> <span class=\"usb dvd vm upgrade"
"\">image that you will use in the next step.</span> <span class=\"download-"
"only-img download-only-iso\">image.</span>"
msgstr ""

#. type: Content of: <div><h2>
msgid "Verify using OpenPGP (optional)"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"If you know OpenPGP, you can also verify your download using an OpenPGP "
"signature instead of, or in addition to, our browser extension or BitTorrent."
msgstr ""

#. type: Content of: <div><ol><li><p>
msgid ""
"Download the <a class=\"usb upgrade download-only-img\" href=\"[[!inline "
"pages=\"inc/stable_amd64_img_sig_url\" raw=\"yes\" sort=\"age\"]]\">OpenPGP "
"signature for the Tails [[!inline pages=\"inc/stable_amd64_version\" raw="
"\"yes\" sort=\"age\"]] USB image</a> <a class=\"dvd vm download-only-iso\" "
"href=\"[[!inline pages=\"inc/stable_amd64_iso_sig_url\" raw=\"yes\" sort="
"\"age\"]]\">OpenPGP signature for the Tails [[!inline pages=\"inc/"
"stable_amd64_version\" raw=\"yes\" sort=\"age\"]] ISO image</a> and save it "
"to the same folder where you saved the image."
msgstr ""

#. type: Content of: <div><h3>
msgid "Basic OpenPGP verification"
msgstr ""

#. type: Content of: <div>
msgid ""
"[[!toggle id=\"basic-openpgp\" text=\"See instructions for basic OpenPGP "
"verification.\"]] [[!toggleable id=\"basic-openpgp\" text=\"\"\" <span class="
"\"hide\">[[!toggle id=\"basic-openpgp\" text=\"\"]]</span>"
msgstr ""

#. type: Content of: <div><p>
msgid "This section provides simplified instructions:"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"#windows\">In Windows with <span class=\"application\">Gpg4win</"
"span></a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"#mac\">In macOS with <span class=\"application\">GPGTools</span></"
"a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "<a href=\"#tails\">In Tails</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "<a href=\"#command-line\">Using the command line</a>"
msgstr ""

#. type: Content of: <div>
msgid "<a id=\"windows\"></a>"
msgstr ""

#. type: Content of: <div><h3>
msgid "In Windows with <span class=\"application\">Gpg4win</span>"
msgstr ""

#. type: Content of: <div><ol><li><p>
msgid ""
"Download the [[Tails signing key|tails-signing.key]] and import it into "
"<span class=\"application\">Gpg4win</span>."
msgstr ""

#. type: Content of: <div><ol><li><p>
msgid ""
"See the [[<span class=\"application\">Gpg4win</span> documentation on "
"importing keys|https://www.gpg4win.org/doc/en/gpg4win-compendium_15.html]]."
msgstr ""

#. type: Content of: <div><ol><li><p>
msgid "Verify the signature of the image that you downloaded."
msgstr ""

#. type: Content of: <div><ol><li><p>
msgid ""
"See the [[<span class=\"application\">Gpg4win</span> documentation on "
"verifying signatures|http://www.gpg4win.org/doc/en/gpg4win-compendium_24."
"html#id4]]."
msgstr ""

#. type: Content of: <div><ol><li><p>
msgid ""
"Verify that the date of the signature is at most five days earlier than the "
"latest version: [[!inline pages=\"inc/stable_amd64_date\" raw=\"yes\" sort="
"\"age\"]]."
msgstr ""

#. type: Content of: <div><ol><li><p>
msgid "If the following warning appears:"
msgstr ""

#. type: Content of: <div><ol><li><pre>
#, no-wrap
msgid ""
"    Not enough information to check the signature validity.\n"
"    Signed on ... by tails@boum.org (Key ID: 0x58ACD84F\n"
"    The validity of the signature cannot be verified.\n"
"    "
msgstr ""

#. type: Content of: <div><ol><li><p>
msgid ""
"Then the image is still correct according to the signing key that you "
"downloaded. To remove this warning you need to <a href=\"#wot\">authenticate "
"the signing key through the OpenPGP Web of Trust</a>."
msgstr ""

#. type: Content of: <div>
msgid "<a id=\"mac\"></a>"
msgstr ""

#. type: Content of: <div><h3>
msgid "In macOS using <span class=\"application\">GPGTools</span>"
msgstr ""

#. type: Content of: <div><ol><li><p>
msgid ""
"Download the [[Tails signing key|tails-signing.key]] and import it into "
"<span class=\"application\">GPGTools</span>."
msgstr ""

#. type: Content of: <div><ol><li><p>
msgid ""
"See the [[<span class=\"application\">GPGTools</span> documentation on "
"importing keys|https://gpgtools.tenderapp.com/kb/gpg-keychain-faq/how-to-"
"find-public-keys-of-your-friends-and-import-them#import-key-file]]."
msgstr ""

#. type: Content of: <div><ol><li><p>
msgid ""
"Open <span class=\"application\">Finder</span> and navigate to the folder "
"where you saved the image and the signature."
msgstr ""
