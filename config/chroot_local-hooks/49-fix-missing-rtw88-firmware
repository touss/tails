#!/bin/sh

set -e
set -u

echo "Checking for possibly missing rtw88 firmware (#17323)"

# Decide where to copy from, and what:
WORKAROUNDS_FW_DIR='/tmp/tails-workarounds/linux-firmware'
DIR='rtw88'
FILES='rtw8822b_fw.bin rtw8822c_fw.bin'

# Sanity check:
orig_dir="$WORKAROUNDS_FW_DIR/$DIR"
if [ ! -d "$orig_dir" ]; then
  echo "=> ERROR: Missing origin directory ($orig_dir)"
  exit 1
fi

firmware_dir="/lib/firmware/$DIR"
if [ ! -d "$firmware_dir" ]; then
  echo "=> Missing parent directory ($firmware_dir), creating"
  mkdir -p "$firmware_dir"
fi


for file in $FILES; do
  path="$firmware_dir/$file"
  if [ ! -f "$path" ]; then
    echo "=> Missing firmware ($file), copying"
    cp "$orig_dir/$file" "$path"
  else
    echo "=> ERROR: Firmware for $file found, maybe this hook could be dropped"
    exit 1
  fi
done
